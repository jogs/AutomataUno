##
# * Operador.rb (version para gema)
# = Programa Que almacena alfabetos y sus respectivos lenguajes, y opera con estos ultimos
#
# Autor::  Joaquín García Santiago
# Web::    https://gitlab.com/u/jogs
# Origen:: https://gitlab.com/jogs/AutomataUno
# 
# == Descripción:
# Este programa reliza lo siguienete
# * Almacena 2 alfabetos
# * Almacena un lenguaje correspondiente a cada alfabeto
# * Opera con los lenguajes ya almacenados
# === Operaciones:
# * Union
# * Concatenación
# * Estrella de kleene
# * Resta
# * -permite visualizar alfabetos y lenguajes
#

##
# La clase automata se encarga de realizar todo el proceso	
class Operador
	##
	# Atributos de tipo protected
	protected
	##
	# @alfabeto1: string
	@alfabeto1
	##
	# @alfabeto2: string
	@alfabeto2
	##
	# @lenguaje1: string
	@lenguaje1
	##
	# @lenguaje1: string
	@lenguaje2

	##
	# Blocke publico
	public
	##
	# Metodo Constructor
	def initialize()
		
	end
	
	##
	# Setter: Alfabeto1 si no se agrega algo, recibe como valores 'a,b'
	def alfabeto1(alfabeto = 'a,b')
		# alfabeto es una cadena?
		correcto = alfabeto.is_a? String
		#si no es correcto hacer Cadena
		unless correcto
			alfabeto = alfabeto.to_s
		end
		@alfabeto1 = alfabeto
		 # Si el alfabeto esta vacio return false
		 if @alfabeto1.empty?
		 	false
		 else
		 	true
		 end #cierra if
	end
	##
	# Setter: Lenguaje1
	def lenguaje1(lenguaje = 'aa,ab,ba,bb')
		# si el lenguaje coinside con el alfabeto
		if lenguajeCorrecto(@alfabeto1, lenguaje).start_with?("Correcto")
			@lenguaje1 = lenguaje
			#return
			true
		else
			false
		end
	end# cierra el metodo Lenguaje1
	
	##
	# Setter: Alfabeto2 si no se agrega algo, recibe como valores 'b,c'
	def alfabeto2(alfabeto = 'b,c')
		# alfabeto es una cadena?
		correcto = alfabeto.is_a? String
		#si no es correcto hacer Cadena
		unless correcto
			alfabeto = alfabeto.to_s
		end
		@alfabeto2 = alfabeto
		 # Si el alfabeto esta vacio return false
		 if @alfabeto2.empty?
		 	false
		 else
		 	true
		 end #cierra if
	end #cierra alfabeto

	##
	# Setter: Lenguaje2
	def lenguaje2(lenguaje = 'bb,bc,cb,cc')
		# si el lenguaje coinside con el alfabeto
		if lenguajeCorrecto(@alfabeto2, lenguaje).start_with?("Correcto")
			@lenguaje2 = lenguaje
			#return
			true
		else
			false
		end
	end # cierra el metodo Lenguaje1

	##
	# Getter: Alfabeto1
	def verAlfabeto1()
		mensaje = "{#{@alfabeto1}}"
	end

	##
	# Getter: Alfabeto2
	def verAlfabeto2()
		mensaje = "{#{@alfabeto2}}"
	end
	##
	# Getter: Lenguaje1
	def verLenguaje1()
		mensaje = "{#{@lenguaje1}}"
	end

	##
	# Getter: Lenguaje2
	def verLenguaje2()
		mensaje = "{#{@lenguaje2}}"
	end
	##
	# blocke protegido
	protected
	##
	# Metodo para comparar si un lenguaje coresponde a un alfabeto
	def lenguajeCorrecto(alfabeto, lenguaje)
		alfa = alfabeto.split(',')
		leng = lenguaje.split(',')
		todos = 0
		# cada elemento del lenguaje es un elemento x
		leng.each do |x|
			coinseden = 0
			#cada caracter del un elemento x es i
			x.each_char do |i|
				# cada caracter del alfabeto es j
				alfa.each do |j|
					if i==j #si la j es igual que un elemento de i
						coinseden = coinseden + 1
						break
					end
				end #cierra alfa.each
				#si coinsiden todos los elementos de x con algun elemento del alfabeto
      		end #cierra x.each_char
			mensaje = ''
			if coinseden==x.length
				todos = todos + 1
				mensaje += "Se acepta {#{x}}\n" 
			else
				mensaje += "No se acepta {#{x}}\n"
			end
   		end #cierra leng.each
   		#return
  		if todos==leng.length
      		mensaje = "Correcto:\n" + mensaje
  		else
  			mensaje = "Incorrecto:\n" + mensaje
  		end
	end #cierra metodo lenguajeCorrecto

	##
	# Metodo para la operacion union
	def unir
		uno = @lenguaje1.split(',')
		dos = @lenguaje2.split(',')

		resultado = ''
		# comparando que no se repitan elementos
		dos.each do |x|
			duplicado = false
			uno.each do |y|
				if x==y
					duplicado = true
				end
			end
			# si no esta duplicado
			unless duplicado
				resultado += (',' + x)
			end
		end

		mensaje = "{#{@lenguaje1}" + "#{resultado}}\n"
	end

	##
	# metodo para preguntar un numero mayor a 0
	def cuantas?
		begin
			puts "¿Para cuantos elementos?: "
			veces = gets.chomp
			veces = veces.to_i
		end while veces<1
		#return
		veces
	end

	##
	# metodo que ejecuta la estrella de kleene
	def estrella(lenguaje, veces)
		if veces == 1
			mensaje = "{/}"
		else
			mensaje = "{"
			veces.times do |x| 
				case x
				when 0 then mensaje += '/'
				when 1 then mensaje += ",#{lenguaje}"
				when 2 then 
					#concatena lenguaje con si mismo
					@ultimo = concatenar(lenguaje, lenguaje)
					mensaje += ',', @ultimo
				else
					#concatena el paso anterior con lenguaje
					@ultimo = concatenar(@ultimo, lenguaje)
					mensaje += ',', @ultimo
				end #cierra case
			end #cierra ciclo veces
			mensaje += "}"
		end #cierra if
	end
	##
	# concatena dos lenguajes y retorna el resultado
	def concatenar(lenguaje1, lenguaje2)
		uno = lenguaje1.split(',')
		dos = lenguaje2.split(',')
		resultado = ''
		# concatenando ambos lenguajes
		uno.each do |x|
			dos.each do |y|
				resultado += (x + y + ',')
			end
		end
			#borra el ultimo caracter
			resultado[-1] = ''
		#retorna
		resultado
	end

	##
	# El metodo Restar permite hacer la sustraccion entre lenguajes
	def restar(lenguaje1, lenguaje2)
		uno = lenguaje1.split(',')
		dos = lenguaje2.split(',')
		resultado = '{'
		# comparando que no se repitan elementos
		uno.each do |x|
			duplicado = false
			dos.each do |y|
				if x==y
					duplicado = true
				end
			end #cierra dos do
			# si no esta duplicado
			unless duplicado
				resultado += (x + ',')
			end
		end #cierra uno do
		if resultado == '{'
			resultado = '{/}'
		else
			resultado[-1] = '}'
		end
		#return
		resultado
	end
end #cierre de clase