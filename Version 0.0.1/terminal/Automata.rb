##
# * Automata.rb
# = Programa Que almacena alfabetos y sus respectivos lenguajes, y opera con estos ultimos
#
# Autor::  Joaquín García Santiago
# Web::    https://gitlab.com/u/jogs
# Origen:: https://gitlab.com/jogs/AutomataUno
# 
# == Descripción:
# Este programa reliza lo siguienete
# * Almacena 2 alfabetos
# * Almacena un lenguaje correspondiente a cada alfabeto
# * Opera con los lenguajes ya almacenados
# === Operaciones:
# * Union
# * Concatenación
# * Estrella de kleene
# * Resta
# * -permite visualizar alfabetos y lenguajes
#

##
# La clase automata se encarga de realizar todo el proceso	
class Automata
	##
	# Blocke publico
	public
	##
	# Metodo Constructor
	def initialize()
		puts "\tBienvenido"
		puts 'Instrucciones: Capture el alfabeto para ambos casos asi como los lenguajes, '
		print "separe cada elemento de los antes mencionados con una coma (,) sin espacios\n\n"
		2.times do |i|
			begin
				puts "Ingrese el Alfabeto #{i+1}: "
				aux = gets.chomp
				if aux==''
					correcto = 'N'
				else
					puts "El alfabeto #{aux} es correcto? (s/n)"
					correcto = gets.upcase.chomp
				end
			end while correcto != 'S' #cierra while

			begin
				puts "Ingrese el Lenguaje #{i+1}: "
				aux2 = gets.chomp
				if aux2 == ''
					correcto = 'N'
				else
					if lenguajeCorrecto(aux, aux2)=='SI'
						puts "El Lenguaje #{aux2} es correcto? (s/n)"
						correcto = gets.upcase.chomp
					else
						puts "El lenguaje no coinside con el Alfabeto"
						correcto = 'N'
					end #cierra if interno
				end # cierra if externo
			end while correcto != 'S' #cierra while
			# guarda alfabeto y lenguaje
			case i
			when 0 then 
				@alfabeto1 = aux
				@lenguaje1 = aux2
			else
				@alfabeto2 = aux
				@lenguaje2 = aux2
			end #cierra case
		end
	end
	
	##
	# Metodo para comparar si un lenguaje coresponde a un alfabeto
	def lenguajeCorrecto(alfabeto, lenguaje)
		alfa = alfabeto.split(',')
		leng = lenguaje.split(',')
		todos = 0
		# cada elemento del lenguaje es un elemento x
		leng.each do |x|
			coinseden = 0
			#cada caracter del un elemento x es i
			x.each_char do |i|
				# cada caracter del alfabeto es j
				alfa.each do |j|
					if i==j #si la j es igual que un elemento de i
						coinseden = coinseden + 1
						break
					end
				end #cierra alfa.each
				#si coinsiden todos los elementos de x con algun elemento del alfabeto
      		end #cierra x.each_char
			
			if coinseden==x.length
				todos = todos + 1
				puts "Se acepta {#{x}}" 
			else
				puts "No se acepta {#{x}}"
			end
      		
   		end #cierra leng.each

  		if todos==leng.length
  			'SI'
  		else
  			'NO'
  		end
	end #cierra metodo lenguajeCorrecto

	##
	# Metodo para preguntar que operacion se decea realizar
	def menu
		begin
			system 'clear'
		puts "     Operaciones con lenguajes     "
		puts "============== Menu ================="
		puts "|   1) Lx U Ly       (union)        |"
		puts "|   2) LxLy          (concatenacion)|"
		puts "|   3) Lx*           (kleene)       |"
		puts "|   4) Lx-Ly         (resta)        |"
		puts "|   5)   Ver Alfabetos              |"
		puts "|   6)   Ver Lenguajes              |"
		puts "|   7)   Salir                      |"
		puts "=====================================\n"
		puts " Elija una opcion: "
		opcion = gets.chomp

		case opcion
		when '1' then union
		when '2' then concatenacion
		when '3' then kleene
		when '4' then resta
		when '5' then alfabetos
		when '6' then lenguajes
		when '7' then puts "Saliendo..."
		else	puts "Usted tecleo una opcion que no es reconocida\nReintente por favor..."
		end # cierra el case
		
		end while opcion != '7' # cierra el while del menu
	end #cierra menu
	##
	# blocke protegido
	protected
	##
	# Metodo para llamar a la operacion Union
	def union
		begin
				system 'clear'
			puts "     Operaciones con lenguajes     "
			puts "============== Union ================"
			puts "|   1) L1 U L2                      |"
			puts "|   2) L2 U L1                      |"
			puts "|   3)        Regresar              |"
			puts "=====================================\n"
			puts " Elija una opcion: "
			opcion = gets.chomp

			case opcion
			when '1', '2' then unir
			when '3' then puts "Regresando..."
			else puts "Por favor elija una opcion valida"
			end
		end while opcion != '3'
	end

	##
	# Metodo para la operacion union
	def unir
		puts "Esta es la union de ambos lenguajes:"
		
		uno = @lenguaje1.split(',')
		dos = @lenguaje2.split(',')

		resultado = ''
		# comparando que no se repitan elementos
		dos.each do |x|
			duplicado = false
			uno.each do |y|
				if x==y
					duplicado = true
				end
			end
			# si no esta duplicado
			unless duplicado
				resultado += (',' + x)
			end
		end

		#unless resultado==''
		#	resultado.insert(0, ',')
		#end

		print "{#{@lenguaje1}"
		print "#{resultado}}\n"
		gets
	end

	##
	# Metodo para la operacion concatenacion
	def concatenacion
		begin
				system 'clear'
			puts "     Operaciones con lenguajes     "
			puts "============ Concatenar ============="
			puts "|   1) L1L2                         |"
			puts "|   2) L2L1                         |"
			puts "|   3)        Regresar              |"
			puts "=====================================\n"
			puts " Elija una opcion: "
			opcion = gets.chomp

			case opcion
			when '1' then puts concatenar(@lenguaje1, @lenguaje2)
			when '2' then puts concatenar(@lenguaje2, @lenguaje1)
			when '3' then puts "Regresando..."
			else puts "Por favor elija una opcion valida"
			end
			gets
		end while opcion != '3'
	end

	##
	# Metodo para la operacion estrella de kleene
	def kleene
		begin
				system 'clear'
			puts "     Operaciones con lenguajes     "
			puts "============== Klenee ================"
			puts "|   1) L1*                          |"
			puts "|   2) L2*                          |"
			puts "|   3)        Regresar              |"
			puts "=====================================\n"
			puts " Elija una opcion: "
			opcion = gets.chomp
			
			case opcion
			when '1' then 
				veces = cuantas?
				estrella(@lenguaje1, veces)
			when '2' then 
				veces = cuantas?
				estrella(@lenguaje2, veces)
			when '3' then puts "Regresando..."
			else puts "Por favor elija una opcion valida"
			end #cierra case
			gets
		end while opcion != '3' #cierra while
	end

	##
	# metodo para preguntar un numero mayor a 0
	def cuantas?
		begin
			puts "¿Para cuantos elementos?: "
			veces = gets.chomp
			veces = veces.to_i
		end while veces<1
		#return
		veces
	end

	##
	# metodo que ejecuta la estrella de kleene
	def estrella(lenguaje, veces)
		if veces == 1
			puts "{/}"
		else
			print "{"
			veces.times do |x| 
				case x
				when 0 then print '/'
				when 1 then print ",#{lenguaje}"
				when 2 then 
					#concatena lenguaje con si mismo
					@ultimo = concatenar(lenguaje, lenguaje)
					print ',', @ultimo
				else
					#concatena el paso anterior con lenguaje
					@ultimo = concatenar(@ultimo, lenguaje)
					print ',', @ultimo
				end #cierra case
			end #cierra ciclo veces
			print "}\n"
		end #cierra if
	end
	##
	# concatena dos lenguajes y retorna el resultado
	def concatenar(lenguaje1, lenguaje2)
		uno = lenguaje1.split(',')
		dos = lenguaje2.split(',')
		resultado = ''
		# concatenando ambos lenguajes
		uno.each do |x|
			dos.each do |y|
				resultado += (x + y + ',')
			end
		end
			#borra el ultimo caracter
			resultado[-1] = ''
		#retorna
		resultado
	end

	##
	# Metodo para la operacion resta
	def resta
		begin
				system 'clear'
			puts "     Operaciones con lenguajes     "
			puts "============== Resta ================"
			puts "|   1) L1 - L2                      |"
			puts "|   2) L2 - L1                      |"
			puts "|   3)        Regresar              |"
			puts "=====================================\n"
			puts " Elija una opcion: "
			opcion = gets.chomp

			case opcion
			when '1' then 
				puts restar(@lenguaje1, @lenguaje2)
				gets
			when '2' then 
				puts restar(@lenguaje2, @lenguaje1)
				gets
			when '3' then puts "Regresando..."
			else puts "Por favor elija una opcion valida"
			end
		end while opcion != '3'
		gets
	end

	##
	# El metodo Restar permite hacer la sustraccion entre lenguajes
	def restar(lenguaje1, lenguaje2)
		uno = lenguaje1.split(',')
		dos = lenguaje2.split(',')
		resultado = '{'
		# comparando que no se repitan elementos
		uno.each do |x|
			duplicado = false
			dos.each do |y|
				if x==y
					duplicado = true
				end
			end #cierra dos do
			# si no esta duplicado
			unless duplicado
				resultado += (x + ',')
			end
		end #cierra uno do
		if resultado == '{'
			resultado = '{/}'
		else
			resultado[-1] = '}'
		end
		#return
		resultado
	end

	##
	# Ver Alfabetos (getter alfabetos)
	def alfabetos
		puts "Alfabeto 1: {#{@alfabeto1}}"
		puts "Alfabeto 2: {#{@alfabeto2}}"
		gets
	end

	##
	# Ver lenguajes (getter lenguajes)
	def lenguajes
		puts "Lenguaje 1: {#{@lenguaje1}}"
		puts "Lenguaje 2: {#{@lenguaje2}}"
		gets
	end
end
#se define un objeto de la clase
Ruby = Automata.new
Ruby.menu
