	class Sample < ActiveRecord::Base
		validates :alfabeto, presence: true
		validates :lenguaje, presence: true, uniqueness: true
	end
