class Operation < ActiveRecord::Base
	validates :sample1, presence: true, length: { minimum: 1 }
	validates :sample2, presence: true, length: { minimum: 1 }
	validates :kleene, presence: true
end
