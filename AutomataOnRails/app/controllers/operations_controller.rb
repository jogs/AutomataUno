class OperationsController < ApplicationController
  before_action :set_operation, only: [:show, :edit, :update, :destroy]

  # GET /operations
  # GET /operations.json
  def index
    @operations = Operation.all
    @samples = Sample.all
  end

  # GET /operations/1
  # GET /operations/1.json
  def show
    @elemento1 = Sample.find(@operation.sample1)
    @elemento2 = Sample.find(@operation.sample2)
    @union = unir(@elemento1.lenguaje, @elemento2.lenguaje)
    @inter = interseccion(@elemento1.lenguaje, @elemento2.lenguaje)
    @concat1 = concatenar(@elemento1.lenguaje, @elemento2.lenguaje)
    @concat2 = concatenar(@elemento2.lenguaje, @elemento1.lenguaje)
    @estrella1 = estrella(@elemento1.lenguaje, @operation.kleene)
    @tamE1 = cuenta(@estrella1)
    @estrella2 = estrella(@elemento2.lenguaje, @operation.kleene)
    @tamE2 = cuenta(@estrella2)
    @resta1 = restar(@elemento1.lenguaje, @elemento2.lenguaje)
    @resta2 = restar(@elemento2.lenguaje, @elemento1.lenguaje)
  end

  # GET /operations/new
  def new
    @operation = Operation.new
    @samples = Sample.all
  end

  # GET /operations/1/edit
  def edit
    @samples = Sample.all
  end

  # POST /operations
  # POST /operations.json
  def create
    @operation = Operation.new(operation_params)

    respond_to do |format|
      if @operation.save
        format.html { redirect_to @operation, notice: 'La Operación se creo con exito' }
        format.json { render :show, status: :created, location: @operation }
      else
        format.html { render :new }
        format.json { render json: @operation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /operations/1
  # PATCH/PUT /operations/1.json
  def update
    respond_to do |format|
      if @operation.update(operation_params)
        format.html { redirect_to @operation, notice: 'La Operación se actulizo con exito' }
        format.json { render :show, status: :ok, location: @operation }
      else
        format.html { render :edit }
        format.json { render json: @operation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /operations/1
  # DELETE /operations/1.json
  def destroy
    @operation.destroy
    respond_to do |format|
      format.html { redirect_to operations_url, notice: 'La operacion se eleminó exitosamente' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_operation
      @operation = Operation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def operation_params
      params.require(:operation).permit(:sample1, :sample2, :kleene)
    end
    ##
    # Metodo para la operacion union
    def unir(lenguaje1, lenguaje2)
      uno = lenguaje1.split(',')
      dos = lenguaje2.split(',')
      arreglo = uno | dos
      resultado = ''
      if ((uno - dos) == uno) && ((dos - uno) == dos)
        resultado = "{/}"
      else
        arreglo.each do |x|
          resultado += (x + ',')
        end
        resultado[-1] = ''
        resultado = "{#{resultado}}"
      end
    end
    ##
    # metodo para la interseccion
    def interseccion(lenguaje1, lenguaje2)
      uno = lenguaje1.split(',')
      dos = lenguaje2.split(',')
      arreglo = uno & dos
      resultado = ''
      if arreglo.length == 0
        resultado = "{/}"
      else
        arreglo.each do |x|
          resultado += (x + ',')
        end
        resultado[-1] = ''
        resultado = "{#{resultado}}"
      end
    end
    ##
    # metodo que ejecuta la estrella de kleene
    def estrella(lenguaje, veces)
      if veces == 1
        mensaje = "{ [ / ] }"
      else
        mensaje = "{ "
        veces.times do |x| 
          case x
          when 0 then mensaje += "[ / ]"
          when 1 then mensaje += ",[#{lenguaje}]"
          when 2 then 
            #concatena lenguaje con si mismo
            @ultimo = concatenar(lenguaje, lenguaje)
            mensaje += (',' + '[ ' + @ultimo + " ]")
          else
            #concatena el paso anterior con lenguaje
            @ultimo = concatenar(@ultimo, lenguaje)
            mensaje += (',' + '[ ' + @ultimo + " ]")
          end #cierra case
        end #cierra ciclo veces
        mensaje += " }"
      end #cierra if
    end
    ##
    # concatena dos lenguajes y retorna el resultado
    def concatenar(lenguaje1, lenguaje2)
      uno = lenguaje1.split(',')
      dos = lenguaje2.split(',')
      resultado = ''
      # concatenando ambos lenguajes
      uno.each do |x|
        dos.each do |y|
          resultado += (x + y + ',')
        end
      end
        #borra el ultimo caracter
        resultado[-1] = ''
      #retorna
      resultado
    end

    ##
    # El metodo Restar permite hacer la sustraccion entre lenguajes
    def restar(lenguaje1, lenguaje2)
      uno = lenguaje1.split(',')
      dos = lenguaje2.split(',')
      resultado = '{'
      # comparando que no se repitan elementos
      uno.each do |x|
        duplicado = false
        dos.each do |y|
          if x==y
            duplicado = true
          end
        end #cierra dos do
        # si no esta duplicado
        unless duplicado
          resultado += (x + ',')
        end
      end #cierra uno do
      if resultado == '{'
        resultado = '{/}'
      else
        resultado[-1] = '}'
      end
      #return
      resultado
    end

    ##
    # metodo para contar los elementos
    def cuenta(elemento)
        arreglo = elemento.split(',')
        arreglo.length
    end
end
