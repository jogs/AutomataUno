class SamplesController < ApplicationController
  before_action :set_sample, only: [:show, :edit, :update, :destroy]

  # GET /samples
  # GET /samples.json
  def index
    @samples = Sample.all
  end

  # GET /samples/1
  # GET /samples/1.json
  def show
  end

  # GET /samples/new
  def new
    @sample = Sample.new
  end

  # GET /samples/1/edit
  def edit
  end

  # POST /samples
  # POST /samples.json
  def create
    @sample = Sample.new(sample_params)
    @sample.alfabeto = limpiar(@sample.alfabeto)
    @sample.lenguaje = limpiar(@sample.lenguaje)
    if (lenguajeCorrecto(@sample.alfabeto, @sample.lenguaje) == "Correcto")
      respond_to do |format|
        if @sample.save
          format.html { redirect_to @sample, notice: 'Los datos se crearon exitosamente' }
          format.json { render :show, status: :created, location: @sample }
        else
          format.html { render :new }
          format.json { render json: @sample.errors, status: :unprocessable_entity }
        end #cierra if
      end #cierra respod_to
    else
      respond_to do |format|
        format.html { render :new }
        format.json { render json: @sample.errors, status: :unprocessable_entity }
      end #cierra respond
    end #cierra if principal
  end #cierra create

  # PATCH/PUT /samples/1
  # PATCH/PUT /samples/1.json
  def update
    @sample.alfabeto = limpiar(@sample.alfabeto)
    @sample.lenguaje = limpiar(@sample.lenguaje)
    if (lenguajeCorrecto(@sample.alfabeto, @sample.lenguaje) == "Correcto")
      respond_to do |format|
        if @sample.update(sample_params)
          format.html { redirect_to @sample, notice: 'Los datos se crearon exitosamente' }
          format.json { render :show, status: :created, location: @sample }
        else
          format.html { render :new }
          format.json { render json: @sample.errors, status: :unprocessable_entity }
        end #cierra if
      end #cierra respod_to
    else
      respond_to do |format|
        format.html { render :new }
        format.json { render json: @sample.errors, status: :unprocessable_entity }
      end #cierra respond
    end #cierra if 
  end #cierra update

  # DELETE /samples/1
  # DELETE /samples/1.json
  def destroy
    @sample.destroy
    respond_to do |format|
      format.html { redirect_to samples_url, notice: 'Los datos se eliminaron exitosamente' }
      format.json { head :no_content }
    end
  end

  protected
  # Verifica que el lenguaje sea coerente con el alfabeto
  def lenguajeCorrecto(alfabeto, lenguaje)
    alfa = alfabeto.split(',')
    leng = lenguaje.split(',')
    todos = 0
    # cada elemento del lenguaje es un elemento x
    leng.each do |x|
      coinseden = 0
      #cada caracter del un elemento x es i
      x.each_char do |i|
        # cada caracter del alfabeto es j
        alfa.each do |j|
          if i==j #si la j es igual que un elemento de i
            coinseden = coinseden + 1
            break
          end #cierra if
        end #cierra alfa.each
      end #cierra x.each_char
      mensaje = ''
      if coinseden==x.length
        todos = todos + 1
      end #cierra if
    end #cierra leng.each
      #return
      if todos==leng.length
          mensaje = "Correcto"
      else
        mensaje = "Incorrecto"
      end
  end #cierra metodo lenguajeCorrecto
  def limpiar(elemento)
    arreglo = elemento.split(',')
    arreglo = arreglo.uniq
    cadena = ''
    arreglo.each do |x|
      cadena += (x + ",")  
    end #cierra do 
    cadena[-1] = ''
    cadena
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sample
      @sample = Sample.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sample_params
      params.require(:sample).permit(:alfabeto, :lenguaje)
    end
end
