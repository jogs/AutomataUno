json.array!(@samples) do |sample|
  json.extract! sample, :id, :alfabeto, :lenguaje
  json.url sample_url(sample, format: :json)
end
