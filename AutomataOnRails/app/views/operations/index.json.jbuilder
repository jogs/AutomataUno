json.array!(@operations) do |operation|
  json.extract! operation, :id, :sample1, :sample2, :kleene
  json.url operation_url(operation, format: :json)
end
