class CreateSamples < ActiveRecord::Migration
  def change
    create_table :samples do |t|
      t.string :alfabeto
      t.string :lenguaje

      t.timestamps null: false
    end
  end
end
