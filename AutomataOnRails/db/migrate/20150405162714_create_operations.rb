class CreateOperations < ActiveRecord::Migration
  def change
    create_table :operations do |t|
      t.integer :sample1
      t.integer :sample2
      t.integer :kleene

      t.timestamps null: false
    end
  end
end
